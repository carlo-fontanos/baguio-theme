<?php get_header(); ?>

	<div class = "inner-page-wrapper">
		<div class = "container">
			<div class = "row content">				
				<h1><?php _e( 'Epic 404 - Article Not Found', 'cvftheme' ); ?></h1>
				<p><?php _e( 'The article you were looking for was not found, but maybe try searching below!', 'cvftheme' ); ?></p>
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>