<div class = "footer-wrapper">
	<div class = "container">
		<div class = "row footer">
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
			<cite><?php _e('Copyright', 'cvftheme'); ?> &copy; <a href="<?php echo home_url('/'); ?>"><?php bloginfo( 'name' ); ?></a>, <?php echo date('Y'); ?>. <?php _e('All Rights Reserved', 'cvftheme'); ?>.</cite>
		</div>
	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>



