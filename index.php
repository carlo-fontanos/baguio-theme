<?php get_header(); ?>

	<div class = "inner-page-wrapper">
		<div class = "container">
			<div class = "row content">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h1><?php the_title(); ?></h1>
						<div class="entry-content">
							<?php the_content(); ?>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>