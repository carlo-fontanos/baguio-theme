<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset = "<?php bloginfo( 'charset' ); ?>" />
    <meta name = "viewport" content = "width=device-width" />
    
	<title><?php wp_title('', true, 'right'); ?></title>

    <link rel = "SHORTCUT ICON" href = "favicon.ico" />
	<link rel = "stylesheet" type = "text/css" href = "<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
	<link rel = "stylesheet" type = "text/css" media = "all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel = "pingback" href = "<?php bloginfo( 'pingback_url' ); ?>" />

    <?php
		wp_head();
		global $header_options;
		if (!empty($header_options)) { foreach ($header_options as $key => $value) { $$key = $value; } }
    ?>
</head>

<body <?php body_class(); ?>>

	<div class = "header-wrapper">
		<div class="conatiner-fluid">
			
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
				<a class="navbar-brand" href="#">Navbar</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-content" aria-controls="navbar-content" aria-expanded="false" aria-label="<?php esc_html_e( 'Toggle Navigation', 'theme-textdomain' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbar-content">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'menu_id'        => 'primary-menu',
						'container'      => false,
						'depth'          => 2,
						'menu_class'     => 'navbar-nav ml-auto',
						'walker'         => new Bootstrap_NavWalker(),
						'fallback_cb'    => 'Bootstrap_NavWalker::fallback',
					) );
					?>
				</div>
			</nav>
			
		</div>
	</div>
    
    
        






